package com.truevolve.android.enact.software_barcode_scanner

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.google.android.gms.common.api.CommonStatusCodes
import com.google.android.gms.vision.barcode.Barcode
import com.truevolve.enact.Interpreter
import com.truevolve.enact.controllers.ActivityBaseController
import com.truevolve.enact.exceptions.PolicyException
import org.json.JSONObject
import trikita.log.Log

/**
 * This controller scans various types of barcodes using the device camera.
 * Three different layout files exist for use with this controller.
 * You need to know which layout you're going to see, when you decide on which fields to include in your policy.
 *
 *  For BOOMIN the following specifically applies:
 *          - RESOURCE_TO_LOAD - has to be "BOOMIN" so that the Boomin layout file is inflated.
 *          - BUTTON_IMAGE - if it contains "DRIVER_CARD" a corresponding image will be loaded, otherwise an image for a license disk.
 *          - CANCEL_TEXT
 * For BOOMIN the following DOES NOT apply and will crash the app:
 *          - TITLE_MESSAGE
 *
 *
 *
 *
 * */
class SoftwareBarcodeScanner : ActivityBaseController() {

    override val type = "barcode_scanner"

    private val TITLE_MESSAGE = "title_message"
    private val DISPLAY_MESSAGE = "display_message"
    private val BUTTON_IMAGE = "button_image" // optional, defaults to PDF417 image
    private val BUTTON_TEXT = "button_text"
    private val CANCEL_TEXT = "cancel_text" // cancel text is optional
    private val SCAN_QR = "scan_qr"
    private val SCAN_CODE128 = "scan_code128"
    private val SCAN_DATAMATRIX = "scan_datamatrix"
    private val SCAN_PDF417 = "scan_pdf417"
    private val STORE_AS = "store_as"
    private val STORE_STRING_AS = "store_string_as"
    private val RESOURCE_TO_LOAD = "resource_to_load" //optional to specify which layout file should be inflated.
    private val CAMERA_SOURCE = "camera_source" //optional to specify which camera to use.
    private val ON_SCANNED = "on_scanned"
    private val ON_CANCEL = "on_cancel"
    private val RC_BARCODE_READ = 348

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        var lightBackground = false
        if (stateObj?.has(RESOURCE_TO_LOAD) as Boolean) {

            val callingAppString = stateObj?.getString(RESOURCE_TO_LOAD)

            if (callingAppString?.contains("BOOMIN") as Boolean) {
                setContentView(R.layout.activity_for_boomin)

            } else if (callingAppString.contains("BACKGROUND_AND_BASICS")) {
                lightBackground = false
                setContentView(R.layout.activity_background_and_basics)
            }

        } else {
            //Default to using the basics
            lightBackground = true
            setContentView(R.layout.activity_basics)
        }

        if (stateObj?.has(TITLE_MESSAGE) == true) {
            val titleMessage = findViewById<TextView>(R.id.titleText)
            titleMessage.text = stateObj?.getString(TITLE_MESSAGE)
        }

        val displayMessage = findViewById<TextView>(R.id.displayMessageText)
        displayMessage.text = stateObj?.getString(DISPLAY_MESSAGE)

        var barcodeFormats = 0

        if (stateObj?.has(SCAN_QR) == true && stateObj?.getBoolean(SCAN_QR) == true) {
            Log.d("SCAN_QR")
            val barcodeImage = findViewById<ImageView>(R.id.barcodeImage)
            if (lightBackground) {
                (barcodeImage as ImageView).setImageResource(R.drawable.ic_scan_qr_black)
            } else {
                (barcodeImage as ImageView).setImageResource(R.drawable.ic_scan_qr_white)
            }
            barcodeFormats = barcodeFormats.or(Barcode.QR_CODE)
        }

        if (stateObj?.has(SCAN_DATAMATRIX) == true && stateObj?.getBoolean(SCAN_DATAMATRIX) == true) {
            Log.d("SCAN_DATAMATRIX")
            barcodeFormats = barcodeFormats.or(Barcode.DATA_MATRIX)
        }

        if (stateObj?.has(SCAN_PDF417) == true && stateObj?.getBoolean(SCAN_PDF417) == true) {
            Log.d("SCAN_PDF417")
            val barcodeImage = findViewById<ImageView>(R.id.barcodeImage)
            if (lightBackground) {
                (barcodeImage as ImageView).setImageResource(R.drawable.ic_scan_pdf417_black)
            } else {
                (barcodeImage as ImageView).setImageResource(R.drawable.ic_scan_pdf417_white)
            }
            barcodeFormats = barcodeFormats.or(Barcode.PDF417)
        }

        if (stateObj?.has(SCAN_CODE128) == true && stateObj?.getBoolean(SCAN_CODE128) == true) {
            Log.d("SCAN_CODE128")
            val barcodeImage = findViewById<ImageView>(R.id.barcodeImage)
            if (lightBackground) {
                (barcodeImage as ImageView).setImageResource(R.drawable.ic_scan_code128_black)
            } else {
                (barcodeImage as ImageView).setImageResource(R.drawable.ic_scan_code128_white)
            }
            barcodeFormats = barcodeFormats.or(Barcode.CODE_128)
        }

        val scanBarcode = findViewById<Button>(R.id.scanBarcode)

        if (stateObj?.has(BUTTON_TEXT) == true) {
            scanBarcode.text = stateObj?.getString(BUTTON_TEXT)
        }

        scanBarcode.setOnClickListener {
            val intent = Intent(applicationContext, BarcodeCaptureActivity::class.java)

            if (stateObj?.has(CAMERA_SOURCE) == true) {
                if (stateObj?.getString(CAMERA_SOURCE).equals(other = "FRONT", ignoreCase = true)) {
                    intent.putExtra(BarcodeCaptureActivity.CameraFrontOrBack, "front")
                }
            }

            intent.putExtra(BarcodeCaptureActivity.AutoFocus, true)
            intent.putExtra(BarcodeCaptureActivity.UseFlash, false)
            intent.putExtra(BarcodeCaptureActivity.BarcodeFormats, barcodeFormats)

            startActivityForResult(intent, RC_BARCODE_READ)
        }

        if (stateObj?.has(BUTTON_IMAGE) == true
                && stateObj?.getString(BUTTON_IMAGE).equals(other = "DRIVER_CARD", ignoreCase = true)) {
            scanBarcode.setBackgroundResource(R.drawable.scan_driver_card_button)
        }

        val cancelButton = findViewById<Button>(R.id.cancelButton)
        cancelButton.setOnClickListener {
            stateObj?.getString(ON_CANCEL)?.let { goToState(it) }
        }

        if (stateObj?.has(CANCEL_TEXT) == true) {
            cancelButton.text = stateObj?.getString(CANCEL_TEXT)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == RC_BARCODE_READ && resultCode == CommonStatusCodes.SUCCESS) {

            if (data != null && data.hasExtra(BarcodeCaptureActivity.BarcodeObject)) {
                val barcode = data.getParcelableExtra<Barcode>(BarcodeCaptureActivity.BarcodeObject)

                val bytes = barcode?.rawValue?.toByteArray(Charsets.UTF_8) ?: ""

                stateObj?.getString(STORE_AS)?.let { Interpreter.dataStore.put(it, bytes) }

                if (stateObj?.has(STORE_STRING_AS) == true) {
                    stateObj?.getString(STORE_STRING_AS)?.let {
                        Interpreter.dataStore.put(it, barcode?.rawValue ?: "")
                    }
                }

                stateObj?.getString(ON_SCANNED)?.let { goToState(it) }
            } else {
                Log.e("onActivityResult: data intent is either null or doesn't have the BarcodeObject.")
                super.onActivityResult(requestCode, resultCode, data)
            }

        } else {
            Log.e("NOP")
            Log.d("Probably handling onBackPressed from BarcodeCaptureActivity")
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun validate(stateObj: JSONObject) {
        if (!stateObj.has(DISPLAY_MESSAGE)) {
            val msg = "$type's $DISPLAY_MESSAGE was not specified"
            Log.e(msg)
            throw PolicyException(msg)

        } else if (!stateObj.has(STORE_AS)) {
            val msg = "$type's $STORE_AS was not specified"
            Log.e(msg)
            throw PolicyException(msg)

        } else if (!stateObj.has(ON_SCANNED)) {
            val msg = "$type's $ON_SCANNED was not specified"
            Log.e(msg)
            throw PolicyException(msg)

        } else if (!stateObj.has(ON_CANCEL)) {
            val msg = "$type's $ON_CANCEL was not specified"
            Log.e(msg)
            throw PolicyException(msg)

        } else if (!(stateObj.has(SCAN_DATAMATRIX) && stateObj.getBoolean(SCAN_DATAMATRIX))
                && !(stateObj.has(SCAN_QR) && stateObj.getBoolean(SCAN_QR))
                && !(stateObj.has(SCAN_PDF417) && stateObj.getBoolean(SCAN_PDF417))
                && !(stateObj.has(SCAN_CODE128) && stateObj.getBoolean(SCAN_CODE128))) {

            val msg = "$type must have at least one of $SCAN_DATAMATRIX, $SCAN_PDF417, $SCAN_CODE128 or $SCAN_QR specified"
            Log.e(msg)
            throw PolicyException(msg)
        }
    }
}
