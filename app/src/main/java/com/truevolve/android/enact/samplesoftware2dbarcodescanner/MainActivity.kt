package com.truevolve.android.enact.samplesoftware2dbarcodescanner

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.truevolve.android.enact.software_barcode_scanner.SoftwareBarcodeScanner
import com.truevolve.enact.Interpreter
import com.truevolve.enact.controllers.ActivityBaseController
import java.util.*

class MainActivity : AppCompatActivity() {

    val policy =
            "{\n" +
                    "  \"start\": {\n" +
                    "    \"type\": \"menu\",\n" +
                    "    \"on_button\": \"read_a_barcode\"\n" +
                    "  },\n" +
                    "  \"read_a_barcode\": {\n" +
                    "    \"type\": \"barcode_scanner\",\n" +
                    "    \"on_scanned\": \"summary\",\n" +
                    "    \"store_as\": \"barcode_read\",\n" +
                    "    \"store_string_as\": \"barcode_read_string\",\n" +
                    "    \"title_message\": \"Setup Bluetooth printer\",\n" +
                    "    \"display_message\": \"Scan the printer's Bluetooth MAC address barcode.\",\n" +
                    "    \"button_text\": \"Scan barcode\",\n" +
                    "    \"resource_to_load\": \"BACKGROUND_AND_BASICS\",\n" +
                    "    \"on_back_pressed\": \"start\",\n" +
                    "    \"on_cancel\": \"start\",\n" +
                    "    \"scan_pdf417\": true\n" +
                    "  },\n" +
                    "  \"summary\": {\n" +
                    "    \"type\": \"summary\",\n" +
                    "    \"on_done\": \"SUCCESS\",\n" +
                    "    \"barcode_data\": \"barcode_read\",\n" +
                    "    \"display_message\": \"Barcode info read is: \",\n" +
                    "    \"on_back_pressed\": \"read_a_barcode\"\n" +
                    "  }\n" +
                    "}"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val listOfControllers = ArrayList<Class<out ActivityBaseController>>()
        listOfControllers.add(Menu::class.java)
        listOfControllers.add(SoftwareBarcodeScanner::class.java)
        listOfControllers.add(Summary::class.java)

        val interp = Interpreter.setup(MainActivity::class.java, policy, listOfControllers)
        interp.start(this)
    }
}
