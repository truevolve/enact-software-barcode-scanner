package com.truevolve.android.enact.samplesoftware2dbarcodescanner;

import android.os.Bundle;
import android.view.View;

import com.truevolve.enact.Interpreter;
import com.truevolve.enact.controllers.ActivityBaseController;
import com.truevolve.enact.exceptions.InterpreterException;
import com.truevolve.enact.exceptions.PolicyException;

import org.json.JSONException;
import org.json.JSONObject;

import trikita.log.Log;

public class Menu extends ActivityBaseController {

    private static final String ON_BUTTON = "on_button";
    private static final String CONTROLLER_TYPE = "menu";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        findViewById(R.id.readButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    goToState(getStateObj().getString(ON_BUTTON));
                } catch (InterpreterException | JSONException e) {
                    e.printStackTrace();
                    Interpreter.INSTANCE.error(Menu.this, e);
                }

            }
        });
    }

    @Override
    public String getType() {
        return CONTROLLER_TYPE;
    }

    @Override
    public void validate(JSONObject stateObj) throws PolicyException {
        if (!stateObj.has(ON_BUTTON)) {
            Log.e("validate: state object does not have " + ON_BUTTON + " which is mandatory");
            throw new PolicyException("State object does not have " + ON_BUTTON + " which is mandatory");
        }
    }
}
