package com.truevolve.android.enact.samplesoftware2dbarcodescanner;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.truevolve.enact.Interpreter;
import com.truevolve.enact.controllers.ActivityBaseController;
import com.truevolve.enact.exceptions.InterpreterException;
import com.truevolve.enact.exceptions.ObjectNotFoundException;
import com.truevolve.enact.exceptions.PolicyException;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.InvocationTargetException;

import trikita.log.Log;

public class Summary extends ActivityBaseController {

    private static final String ON_DONE = "on_done";
    private static final String BARCODE_DATA = "barcode_data";
    private static final String DISPLAY_MESSAGE = "display_message";

    private static final String CONTROLLER_TYPE = "summary";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);

        TextView display = findViewById(R.id.displayMessageText);
        TextView barData = findViewById(R.id.barcodeDataText);
        Button doneButton = findViewById(R.id.doneButton);

        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    goToState(getStateObj().getString(ON_DONE));
                } catch (InterpreterException | JSONException e) {
                    e.printStackTrace();
                    Interpreter.INSTANCE.error(Summary.this, e);
                }
            }
        });

        try {
            display.setText(getStateObj().getString(DISPLAY_MESSAGE));

            byte[] barResult = Interpreter.INSTANCE.getDataStore().getProperty(getStateObj().getString(BARCODE_DATA));

            if (barResult != null) {
                barData.setText(new String(barResult));
            } else {
                barData.setText("No barcode data available");
            }

        } catch (JSONException | NoSuchMethodException | ObjectNotFoundException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            Interpreter.INSTANCE.error(Summary.this, e);
        }
    }

    @Override
    public String getType() {
        return CONTROLLER_TYPE;
    }

    @Override
    public void validate(JSONObject stateObj) throws PolicyException {
        if (!stateObj.has(DISPLAY_MESSAGE)) {
            Log.e("validate: " + DISPLAY_MESSAGE + " was not specified.");
            throw new PolicyException(DISPLAY_MESSAGE + " was not specified.");
        } else if (!stateObj.has(ON_DONE)) {
            Log.e("validate: " + ON_DONE + " was not specified.");
            throw new PolicyException(ON_DONE + " was not specified.");
        } else if (!stateObj.has(BARCODE_DATA)) {
            Log.e("validate: " + BARCODE_DATA + " was not specified.");
            throw new PolicyException(BARCODE_DATA + " was not specified.");
        }
    }
}
